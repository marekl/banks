package banks

//go:generate go run src/generator.go

type Bank struct {
	Code string `json:"code"`
	Name string `json:"name"`
	BIC  string `json:"BIC"`
}
