VERSION=$(strip $(shell node -p "require('./package.json').version"))
VALID_SINCE=$(strip $(shell node -p "Object.values(require('./package.json').sources).map(s => s.validSince).sort().pop()"))

PACKAGE=gitlab.com/marekl/banks
NPM_PACKAGE=@marekl/banks

EXECUTABLES=go git npm
K := $(foreach exec,$(EXECUTABLES),\
	$(if $(shell which $(exec)),ok,$(error "No $(exec) in PATH")))

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

.PHONY: default clean generate publish check

default: generate

generate:
	go generate $(PACKAGE)/...

publish: clean generate
	npm publish
	git add .
	git commit -m "Release $(VERSION) with data valid since $(VALID_SINCE)"
	git tag -a "v$(VERSION)" -m "Release $(VERSION)"
	git push origin
	git push origin v$(VERSION)

clean: ## Remove previous build files
	rm -f cz-banks-data.go
	rm -rf lib/*

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'