
export interface Bank {
    code: string
    name: string
    BIC: string
}

export declare const CZBanks: Bank[];
export declare const SKBanks: Bank[];
